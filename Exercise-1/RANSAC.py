# Import PCL module
import pcl


#################### Voxel Filter ##########################
def voxel_filter(cloud, save_file=True):
    # Create a VoxelGrid filter object for our input point cloud
    vox = cloud.make_voxel_grid_filter()

    # Choose a voxel (also known as leaf) size
    # Note: this (1) is a poor choice of leaf size
    # Experiment and find the appropriate size!
    LEAF_SIZE = 0.01

    # Set the voxel (or leaf) size
    vox.set_leaf_size(LEAF_SIZE, LEAF_SIZE, LEAF_SIZE)

    # Call the filter function to obtain the resultant downsampled point cloud
    cloud_filtered = vox.filter()
    if save_file:
        filename = 'voxel_downsampled.pcd'
        pcl.save(cloud_filtered, filename)

    return cloud_filtered


#################### PassThrough Filter ##########################
def passthrough_filter(cloud, save_file=True):
    # Create a PassThrough filter object.
    passthrough = cloud.make_passthrough_filter()

    # Assign axis and range to the passthrough filter object.
    filter_axis = 'z'
    passthrough.set_filter_field_name(filter_axis)
    axis_min, axis_max = 0.77, 1.2
    passthrough.set_filter_limits(axis_min, axis_max)

    # Finally use the filter function to obtain the resultant point cloud.
    cloud_filtered = passthrough.filter()
    if save_file:
        filename = 'passthrough_filtered.pcd'
        pcl.save(cloud_filtered, filename)

    return cloud_filtered


#################### RANSAC plane segmentation ####################
def RANSAC_plane_segmentation(cloud):
    seg = cloud.make_segmenter()
    seg.set_model_type(pcl.SACMODEL_PLANE)
    seg.set_method_type(pcl.SAC_RANSAC)

    max_distance = 0.01
    seg.set_distance_threshold(max_distance)
    inliers, coefficients = seg.segment()
    return inliers, coefficients


def extract_indices(cloud, inliers, negative=False):
    extracted_indices = cloud.extract(inliers, negative=negative)
    filename = 'extracted_{}.pcd'.format('inliers' if not negative else 'outliers')
    print('extracted_indices:', extracted_indices)
    pcl.save(extracted_indices, filename)
    return extracted_indices


def filter_noisy(cloud):
    # Save pcd for tabletop objects
    outlier_filter = cloud.make_statistical_outlier_filter()
    outlier_filter.set_mean_k(50)
    x = 1
    outlier_filter.set_std_dev_mul_thresh(x)
    cloud_filtered = outlier_filter.filter()
    return cloud_filtered


if __name__ == '__main__':
    # Load Point Cloud file
    cloud = pcl.load_XYZRGB('tabletop.pcd')

    voxel_filtered_cloud = voxel_filter(cloud)

    passthrough_filtered_cloud = passthrough_filter(voxel_filtered_cloud)

    inliers, coefficients = RANSAC_plane_segmentation(passthrough_filtered_cloud)

    extracted_inliers = extract_indices(
        passthrough_filtered_cloud, inliers, negative=False)
    extracted_outliers = extract_indices(
        passthrough_filtered_cloud, inliers, negative=True)
