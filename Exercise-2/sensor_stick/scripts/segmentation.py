#!/usr/bin/env python

# Import modules
from pcl_helper import *
from RANSAC import voxel_filter
from RANSAC import passthrough_filter
from RANSAC import RANSAC_plane_segmentation
from RANSAC import extract_indices


# Callback function for your Point Cloud Subscriber
def pcl_callback(pcl_msg):
    debug = False

    # Convert ROS msg to PCL data
    cloud = ros_to_pcl(pcl_msg)

    # Voxel Grid Downsampling
    voxel_filtered_cloud = voxel_filter(cloud, save_file=debug)

    # PassThrough Filter
    passthrough_filtered_cloud = passthrough_filter(voxel_filtered_cloud,
                                                    save_file=debug)

    # RANSAC Plane Segmentation
    inliers, coefficients = RANSAC_plane_segmentation(passthrough_filtered_cloud)

    # Extract inliers and outliers
    pcl_table = extract_indices(
        passthrough_filtered_cloud, inliers, negative=False, save_file=debug)
    pcl_objects = extract_indices(
        passthrough_filtered_cloud, inliers, negative=True, save_file=debug)

    # Euclidean Clustering
    white_cloud = XYZRGB_to_XYZ(pcl_objects)
    tree = white_cloud.make_kdtree()
    # Create a cluster extraction object
    ec = white_cloud.make_EuclideanClusterExtraction()
    # Set tolerances for distance threshold 
    # as well as minimum and maximum cluster size (in points)
    # NOTE: These are poor choices of clustering parameters
    # Your task is to experiment and find values that work for segmenting objects.
    ec.set_ClusterTolerance(0.05)
    ec.set_MinClusterSize(50)
    ec.set_MaxClusterSize(1000)
    # Search the k-d tree for clusters
    ec.set_SearchMethod(tree)
    # Extract indices for each of the discovered clusters
    cluster_indices = ec.Extract()

    # Create Cluster-Mask Point Cloud to visualize each cluster separately
    # Assign a color corresponding to each segmented object in scene
    # print('len(cluster_indices):', len(cluster_indices))
    cluster_color = get_color_list(len(cluster_indices))
    color_cluster_point_list = []
    for j, indices in enumerate(cluster_indices):
        for i, indice in enumerate(indices):
            color_cluster_point_list.append([white_cloud[indice][0],
                                             white_cloud[indice][1],
                                             white_cloud[indice][2],
                                             rgb_to_float(cluster_color[j])])
    #Create new cloud containing all clusters, each with unique color
    cluster_cloud = pcl.PointCloud_PointXYZRGB()
    cluster_cloud.from_list(color_cluster_point_list)

    # Convert PCL data to ROS messages
    ros_cloud_table = pcl_to_ros(pcl_table)
    ros_cloud_objects = pcl_to_ros(pcl_objects)
    ros_cluster_cloud = pcl_to_ros(cluster_cloud)

    # Publish ROS messages
    pcl_objects_pub.publish(ros_cloud_objects)
    pcl_table_pub.publish(ros_cloud_table)
    pcl_cluster_pub.publish(ros_cluster_cloud)


if __name__ == '__main__':
    # ROS node initialization
    rospy.init_node('clustering', anonymous=True)

    # Create Subscribers
    pcl_sub = rospy.Subscriber(
        '/sensor_stick/point_cloud', pc2.PointCloud2, pcl_callback,
        queue_size=1)

    # Create Publishers
    pcl_objects_pub = rospy.Publisher(
        '/pcl_objects', PointCloud2, queue_size=1)
    pcl_table_pub = rospy.Publisher(
        '/pcl_table', PointCloud2, queue_size=1)
    pcl_cluster_pub = rospy.Publisher(
        '/pcl_cluster', PointCloud2, queue_size=1)

    # Initialize color_list
    get_color_list.color_list = []

    # Spin while node is not shutdown
    while not rospy.is_shutdown():
        rospy.spin()

